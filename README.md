# Robot toy Web 

Robot toy web application

## Getting Started

### Prerequisites

To run the project you should have docker and docker-compose installed.

Get master.key attached in the email and copy to the config folder.

After that

```
docker-compose up
```
OR
```
rails server
```

## Authors

* **Jonhnes Lopes** - [jonhnes](https://github.com/jonhnes)